# LoRa Access Control

## Overview

### Owners & Administrators

| Title       | Details                                                                                                      |
|-------------|--------------------------------------------------------------------------------------------------------------|
| **E-group** | [lora-en-access-control-admin](https://groups-portal.web.cern.ch/group/lora-en-access-control-admin/details) |
| **People**  | [Gregory Godineau](https://phonebook.cern.ch/search?q=Gregory-Godineau)                                      |

### Kafka Topics

| Environment    | Topic Name                                                                                                              |
|----------------|-------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-access-control](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-access-control)                 |
| **Production** | [lora-access-control-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-access-control-decoded) |
| **QA**         | [lora-access-control](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-access-control)                 |
| **QA**         | [lora-access-control-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-access-control-decoded) |

### Configuration

| Title                        | Details                                                                                                |
|------------------------------|--------------------------------------------------------------------------------------------------------|
| **Application Name**         | lora-EN-access-control                                                                                 |
| **Configuration Repository** | [app-configs/lora-access-control](https://gitlab.cern.ch/nile/streams/app-configs/lora-access-control) |
