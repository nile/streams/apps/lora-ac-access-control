package ch.cern.nile.app;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

import ch.cern.nile.app.generated.AccessControlPacket;
import ch.cern.nile.common.schema.SchemaInjector;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class AccessControlStream extends LoraDecoderStream<AccessControlPacket> {

    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("devEUI", "DEVEUI");
        CUSTOM_METADATA_FIELD_MAP.put("timestamp", "TIMESTAMP");
        CUSTOM_METADATA_FIELD_MAP.put("data", "DATA");
    }

    public AccessControlStream() {
        super(AccessControlPacket.class, CUSTOM_METADATA_FIELD_MAP, true, false, false);
    }

    @Override
    public Map<String, Object> enrichCustomFunction(final Map<String, Object> map, final JsonObject value) {
        return SchemaInjector.inject(map);
    }

}
